package com.epam.news.exception;

public final class NewsManagerException extends Exception {

	static private final long serialVersionUID = 1L;
	
	public NewsManagerException(String msg) {
		super(msg);
	}
	
	public NewsManagerException(Exception ex) {
		super(ex);
	}
}
