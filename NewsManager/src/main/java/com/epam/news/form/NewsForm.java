package com.epam.news.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.epam.news.entity.News;


@SuppressWarnings("serial")
public final class NewsForm extends ActionForm {

	private ArrayList <News> newsList;
	private int id;
	private News currentNews;
	private Integer[] selected;
	private String dateString;
	private String currentAction;
	private String previousAction;
	
	public String getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
	}

	public String getPreviousAction() {
		return previousAction;
	}

	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}

	public ArrayList <News> getNewsList() {
		return newsList;
	}

	public void setNewsList(ArrayList <News> newsList) {
		this.newsList = newsList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public News getCurrentNews() {
		return currentNews;
	}

	public void setCurrentNews(News currentNews) {
		this.currentNews = currentNews;
	}

	public Integer[] getSelected() {
		return selected;
	}

	public void setSelected(Integer[] selected) {
		this.selected = selected;
	}
	
	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
}