package com.epam.news.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.news.connector.DatabaseConnector;
import com.epam.news.exception.NewsManagerException;


public final class ConnectionPool {

	private String driverName;
	private String url;
	private String user;
	private String password;
	private ArrayBlockingQueue <DatabaseConnector> connectors;
	static private final int MAX_CONNECTORS = 10;	
	static private final Log log = LogFactory.getLog(ConnectionPool.class);

	public void init() throws NewsManagerException {
		try {
			Class.forName(this.driverName);
			connectors = new ArrayBlockingQueue<DatabaseConnector>(MAX_CONNECTORS);
			for (int i = 0; i < MAX_CONNECTORS; i++) {
				Connection connection = DriverManager.getConnection(this.url, this.user, this.password);
				DatabaseConnector connector = new DatabaseConnector(this, connection);
				connectors.add(connector);
			}
		} catch (SQLException | ClassNotFoundException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}

	public DatabaseConnector take() throws NewsManagerException {
		try {
			DatabaseConnector connector = connectors.take();
			return connector;
		} catch (InterruptedException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}

	public synchronized void release(DatabaseConnector connector) {
		connectors.add(connector);
	}
	
	public String getDriverName() {
		return driverName;
	}
	
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
