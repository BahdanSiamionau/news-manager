package com.epam.news.action;

import java.sql.Date;

import java.text.SimpleDateFormat;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.News;
import com.epam.news.form.NewsForm;


public final class NewsAction extends DispatchAction {
	
	private INewsDAO newsDAO;
	static private final String VIEW = "view";
	static private final String VIEW_DO = "view.do";
	static private final String NEWS = "news";
	static private final String INDEX = "index";
	static private final String EDIT = "edit";
	static private final String LIST = "list";
	static private final String LOCALE = "locale";
	static private final String TRUE = "true";
	static private final String PROPERTY_NAME = "com.epam.news.properties.labels";
	static private final String LABEL_NAME = "format.date";

	public ActionForward news(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentAction(NEWS);
		newsForm.setNewsList(newsDAO.getNewsList());
		newsForm.setPreviousAction(NEWS);
		return mapping.findForward(NEWS);
	}

	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentAction(VIEW);
		newsForm.setCurrentNews(newsDAO.getCurrentNews(newsForm.getId()));
		newsForm.setPreviousAction(VIEW);
		return mapping.findForward(VIEW);
	}

	public ActionForward edit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentAction(EDIT);
		ResourceBundle labels = ResourceBundle.getBundle(PROPERTY_NAME, (Locale)request.getSession().getAttribute(Globals.LOCALE_KEY));
		SimpleDateFormat dateFormat = new SimpleDateFormat(labels.getString(LABEL_NAME));
		request.getSession().setAttribute(EDIT, request.getParameter(EDIT));
		if (TRUE.equals(request.getParameter(EDIT))) {
			newsForm.setCurrentNews(newsDAO.getCurrentNews(newsForm.getId()));
			newsForm.setDateString(dateFormat.format(newsForm.getCurrentNews().getDate()));
		} else {
			if (newsForm.getCurrentNews() == null) {
				newsForm.setCurrentNews(new News());
			} 
			newsForm.setDateString(dateFormat.format(new Date(System.currentTimeMillis())));
		}
		return mapping.findForward(EDIT);
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		ResourceBundle labels = ResourceBundle.getBundle(PROPERTY_NAME, (Locale)request.getSession().getAttribute(Globals.LOCALE_KEY));
		SimpleDateFormat dateFormat = new SimpleDateFormat(labels.getString(LABEL_NAME));
		newsForm.getCurrentNews().setDate(new Date(dateFormat.parse(newsForm.getDateString()).getTime()));
		if(TRUE.equals(request.getSession().getAttribute(EDIT))) {
			newsDAO.editNews(newsForm.getId(), newsForm.getCurrentNews());
		} else {
			newsDAO.addNews(newsForm.getCurrentNews());
		}
		newsForm.setId(newsForm.getCurrentNews().getId());
		newsForm.setCurrentAction(VIEW);
		return mapping.findForward(VIEW_DO);
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		if(TRUE.equals(request.getParameter(LIST))) {
			newsDAO.removeNews(newsForm.getSelected());
		} else {
			Integer[] selected = {newsForm.getId()};
			newsDAO.removeNews(selected);
		}
		return mapping.findForward(INDEX);
	}
	
	public ActionForward language(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		request.getSession().setAttribute(Globals.LOCALE_KEY, new Locale((String)request.getParameter(LOCALE)));
		return mapping.findForward(newsForm.getCurrentAction());
	}
	
	public ActionForward previous(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentAction(newsForm.getPreviousAction());
		return mapping.findForward(newsForm.getPreviousAction());
	}
	
	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
}