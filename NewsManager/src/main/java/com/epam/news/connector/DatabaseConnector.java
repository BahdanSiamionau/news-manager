package com.epam.news.connector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.news.exception.NewsManagerException;
import com.epam.news.pool.ConnectionPool;


public final class DatabaseConnector implements AutoCloseable {
	
	private ConnectionPool pool;
	private Connection connection;
	static private final String EXCEPTION_NULL_CONN = "Connection is null";
	static private final Log log = LogFactory.getLog(DatabaseConnector.class);

	public DatabaseConnector(ConnectionPool pool, Connection connection) {
		this.pool = pool;
		this.connection = connection;
	}
	
	public Statement getStatement() throws NewsManagerException {
		try {
			if (connection != null) {
				return connection.createStatement();
			}
			throw new NewsManagerException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	public PreparedStatement getPreparedStatement(String query) throws NewsManagerException {
		try {
			if (connection != null) {
				return connection.prepareStatement(query);
			}
			throw new NewsManagerException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	public void close() {
		pool.release(this);
	}
}