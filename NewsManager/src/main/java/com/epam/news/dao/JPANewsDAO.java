package com.epam.news.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.epam.news.entity.News;
import com.epam.news.exception.NewsManagerException;


public final class JPANewsDAO implements INewsDAO {
	
	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public ArrayList <News> getNewsList() throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			Query query = entityManager.createNamedQuery("select_all");
			@SuppressWarnings("unchecked")
			List <News> list = query.getResultList();
			entityManager.getTransaction().commit();
			return new ArrayList <News> (list);
		} finally {
			entityManager.close();
		}
	}

	@Override
	public News getCurrentNews(int id) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			entityManager.getTransaction().commit();
			return entityManager.find(News.class, id);
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void addNews(News news) throws NewsManagerException {
		news.setId(0);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			entityManager.persist(news);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void editNews(int id, News news) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			news.setId(id);
			entityManager.merge(news);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void removeNews(Integer[] selectedItems) throws NewsManagerException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		try {
			Query query = entityManager.createNamedQuery("delete");
			query.setParameter("ID", Arrays.asList(selectedItems));
			query.executeUpdate();
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
}
