package com.epam.news.dao;

import java.util.ArrayList;

import com.epam.news.entity.News;
import com.epam.news.exception.NewsManagerException;


public interface INewsDAO {
		
	public ArrayList <News> getNewsList() throws NewsManagerException;
	public News getCurrentNews(int id) throws NewsManagerException;
	public void addNews(News news) throws NewsManagerException;
	public void editNews(int id, News news) throws NewsManagerException;
	public void removeNews(Integer[] selectedItems) throws NewsManagerException;
}