package com.epam.news.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.news.connector.DatabaseConnector;
import com.epam.news.entity.News;
import com.epam.news.exception.NewsManagerException;
import com.epam.news.pool.ConnectionPool;


public final class JDBCNewsDAO implements INewsDAO {

	static private final String SQL_SELECT_NEWS_ALL = "select * from NEWSTABLE order by NEWS_DATE desc";
	static private final String SQL_SELECT_NEWS = "select * from NEWSTABLE where ID=?";
	static private final String SQL_INSERT_NEWS = "insert into NEWSTABLE values (null, ?, ?, ?, ?)";
	static private final String SQL_UPDATE_NEWS = "update NEWSTABLE set TITLE=?, NEWS_DATE=?, BRIEF=?, CONTENT=? WHERE ID=?";
	static private final String SQL_REMOVE_NEWS = "delete from NEWSTABLE where ID in (";
	static private final String SQL_SEQUENCE_GET_CURVAL = "select NEWS_INCREMENT.CURRVAL from DUAL";
	static private final String ID = "ID";
	static private final String TITLE = "TITLE";
	static private final String DATE = "NEWS_DATE";
	static private final String BRIEF = "BRIEF";
	static private final String CONTENT = "CONTENT";
	static private final Log log = LogFactory.getLog(JDBCNewsDAO.class);
	private ConnectionPool pool;

	public ArrayList <News> getNewsList() throws NewsManagerException {
		ArrayList <News> newsList = new ArrayList <News>();
		try (DatabaseConnector connector = pool.take()) {
			Statement statement = connector.getStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEWS_ALL);
			while (resultSet.next()) {
				News news = new News();
				news.setId(resultSet.getInt(ID));
				news.setTitle(resultSet.getString(TITLE));
				news.setDate(resultSet.getDate(DATE));
				news.setBrief(resultSet.getString(BRIEF));
				news.setContent(resultSet.getString(CONTENT));
				newsList.add(news);
			}
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
		return newsList;
	}

	public News getCurrentNews(int id) throws NewsManagerException {
		News currentNews = new News();
		try (DatabaseConnector connector = pool.take()) {
			PreparedStatement preparedStatement = connector.getPreparedStatement(SQL_SELECT_NEWS);
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				currentNews = new News();
				currentNews.setId(resultSet.getInt(ID));
				currentNews.setTitle(resultSet.getString(TITLE));
				currentNews.setDate(resultSet.getDate(DATE));
				currentNews.setBrief(resultSet.getString(BRIEF));
				currentNews.setContent(resultSet.getString(CONTENT));
			}
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
		return currentNews;
	}

	public void addNews(News news) throws NewsManagerException {
		try (DatabaseConnector connector = pool.take()) {
			PreparedStatement pStatement = connector.getPreparedStatement(SQL_INSERT_NEWS);
			pStatement.setString(1, news.getTitle());
			pStatement.setDate(2, news.getDate());
			pStatement.setString(3, news.getBrief());
			pStatement.setString(4, news.getContent());
			pStatement.executeUpdate();
			
			Statement statement = connector.getStatement();
			ResultSet resultSet = statement.executeQuery(SQL_SEQUENCE_GET_CURVAL);
			if (resultSet.next()) {
				news.setId(resultSet.getInt(1));
			}
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}

	public void editNews(int id, News news) throws NewsManagerException {
		try (DatabaseConnector connector = pool.take()) {
			PreparedStatement statement = connector.getPreparedStatement(SQL_UPDATE_NEWS);
			statement.setString(1, news.getTitle());
			statement.setDate(2, news.getDate());
			statement.setString(3, news.getBrief());
			statement.setString(4, news.getContent());
			statement.setInt(5, id);
			statement.executeUpdate(); 
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		} 
	}

	public void removeNews(Integer[] selectedItems) throws NewsManagerException {
		try (DatabaseConnector connector = pool.take()) {
			StringBuffer query = new StringBuffer(SQL_REMOVE_NEWS);
			for (int i = 0; i < selectedItems.length; i++) {
				if (i != 0) {
					query.append(',');
				}
				query.append('?');	
			}
			query.append(')');
			PreparedStatement statement = connector.getPreparedStatement(query.toString());
			for (int i = 0; i < selectedItems.length; i++) {
				statement.setInt(i + 1, selectedItems[i]);
			}
			statement.executeUpdate();
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	
	public ConnectionPool getPool() {
		return pool;
	}
	
	public void setPool (ConnectionPool pool) {
		this.pool = pool;
	}
} 
