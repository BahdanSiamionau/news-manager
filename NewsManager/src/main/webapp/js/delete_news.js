function deleteFromList(localizedMessages) {
	var agree = confirm(localizedMessages.CONFIRM_DELETE);
	if (agree) {
		return true;
	} else {
		return false;
	}
}

function deleteFromListSome(localizedMessages) {
	var tags = document.getElementsByName("selected");
	var result = false;
	for ( var i = 0; i < tags.length; i++) {
		if (tags[i].checked) {
			result = true;
			break;
		}
	}
	if (!result) {
		alert(localizedMessages.NOTHING_CHECK);
		return false;
	}
	var agree = confirm(localizedMessages.CONFIRM_DELETE);
	if (agree) {
		return true;
	} else {
		return false;
	}
}