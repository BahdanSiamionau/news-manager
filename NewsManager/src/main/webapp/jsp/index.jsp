<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<logic:redirect action="/newsAction.do?method=news" />

<tiles:insert page="/baseLayout.jsp" flush="true">
	<tiles:put name="header" value="/header.jsp" />
	<tiles:put name="menu" value="/menu.jsp" />
	<tiles:put name="body" value="/body.jsp" />
	<tiles:put name="footer" value="/footer.jsp" />
</tiles:insert>