<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<div id="head">
		<tiles:insert attribute="header" ignore="true" />
	</div>

	<div id="menu">
		<tiles:insert attribute="menu" />
	</div>

	<div id="content">
		<tiles:insert attribute="body" />
	</div>

	<div id="foot">
		<tiles:insert attribute="footer" />
	</div>
</body>
</html>
