<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><bean:message key="label.common.edittitle" /></title>
<script src="js/validator.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	function handler(form) {
		var localizedMessages = {
				REQ_TITLE : '<bean:message key="error.message.required.title" />',
				REQ_DATE : '<bean:message key="error.message.required.date" />',
				REQ_BRIEF : '<bean:message key="error.message.required.brief" />',
				REQ_CONTENT : '<bean:message key="error.message.required.content" />',
				MAX_TITLE : '<bean:message key="error.message.maxlength.title" />',
				MAX_BRIEF : '<bean:message key="error.message.maxlength.brief" />',
				MAX_CONTENT : '<bean:message key="error.message.maxlength.content" />',
				WRONG_FORMAT : '<bean:message key="error.message.format.date" />',
				DATE_FORMAT : '<bean:message key="format.date" />'
		};
		return validateNewsForm(form, localizedMessages);
	}
</script>
</head>
<body>
	<html:form action="/newsAction.do?method=update" method="post"
		onsubmit="return handler(this);">
		<logic:equal name="edit" value="true">
			<html:link action="/newsAction.do?method=news">
				<bean:message key="label.common.news" />
			</html:link>
			<bean:message key="label.common.section.edit" />
			<table id="view" cellspacing="20px">
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.title" /></td>
					<td id="view-second-column"><html:text name="newsForm"
							styleId="text-title-style" property="currentNews.title" /></td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.date" /></td>
					<td id="view-second-column"><html:text name="newsForm"
							property="dateString"/></td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.brief" /></td>
					<td id="view-second-column"><html:textarea name="newsForm"
							property="currentNews.brief" styleId="textarea-brief-style" /></td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.content" /></td>
					<td id="view-second-column"><html:textarea name="newsForm"
							property="currentNews.content" styleId="textarea-content-style" />
					</td>
				</tr>
			</table>
			<div id="edit-button">
				<html:submit styleId="edit-button-style">
					<bean:message key="label.common.button.update" />
				</html:submit>
			</div>
		</logic:equal>
		<logic:notEqual name="edit" value="true">
			<html:link action="/newsAction.do?method=news">
				<bean:message key="label.common.news" />
			</html:link>
			<bean:message key="label.common.section.add" />
			<table id="view" cellspacing="20px">
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.title" /></td>
					<td id="view-second-column"><html:text name="newsForm"
							styleId="text-title-style" property="currentNews.title" value="" />
					</td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.date" /></td>
					<td id="view-second-column"><html:text name="newsForm"
							property="dateString" /></td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.brief" /></td>
					<td id="view-second-column"><html:textarea name="newsForm"
							styleId="textarea-brief-style" property="currentNews.brief"
							value="" /></td>
				</tr>
				<tr>
					<td id="view-first-column"><bean:message
							key="label.common.table.content" /></td>
					<td id="view-second-column"><html:textarea name="newsForm"
							styleId="textarea-content-style" property="currentNews.content"
							value="" /></td>
				</tr>
			</table>
			<div id="edit-button">
				<html:submit styleId="edit-button-style">
					<bean:message key="label.common.button.update" />
				</html:submit>
			</div>
		</logic:notEqual>
	</html:form>
	<html:form action="/newsAction.do?method=previous">
		<html:submit styleId="edit-button-style">
			<bean:message key="label.common.button.cancel" />
		</html:submit>
	</html:form>
</body>
</html>