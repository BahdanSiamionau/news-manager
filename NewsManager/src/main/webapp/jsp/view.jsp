<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><bean:message key="label.common.recent" /></title>
<script src="js/delete_news.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	function handler() {
		var localizedMessages = {
			CONFIRM_DELETE: "<bean:message key="confirm.delete" />"
		};
		return deleteFromList(localizedMessages);
	}
</script>
</head>
<body>
	<html:link action="/newsAction.do?method=news">
		<bean:message key="label.common.news" />
	</html:link>
	<bean:message key="label.common.section.view" />
	<table id="view" cellspacing="20px">
		<tr>
			<td id="view-first-column"><bean:message
					key="label.common.table.title" /></td>
			<td id="view-second-column"><bean:write name="newsForm"
					property="currentNews.title" /></td>
		</tr>
		<tr>
			<td id="view-first-column"><bean:message
					key="label.common.table.date" /></td>
			<td id="view-second-column"><bean:write name="newsForm"
					property="currentNews.date" formatKey="format.date" /></td>
		</tr>
		<tr>
			<td id="view-first-column"><bean:message
					key="label.common.table.brief" /></td>
			<td id="view-second-column"><bean:write name="newsForm"
					property="currentNews.brief" /></td>
		</tr>
		<tr>
			<td id="view-first-column"><bean:message
					key="label.common.table.content" /></td>
			<td id="view-second-column"><bean:write name="newsForm"
					property="currentNews.content" /></td>
		</tr>
	</table>
	<div id="delete">
		<div id="floater">
			<html:form action="/newsAction.do?method=delete&list=false" method="post"
				onsubmit="return handler();">
				<html:submit styleId="button-delete">
					<bean:message key="label.common.button.delete" />
				</html:submit>
			</html:form>
		</div>
		<html:form action="/newsAction.do?method=edit&edit=true">
			<html:submit styleId="button-delete">
				<bean:message key="label.common.button.edit" />
			</html:submit>
		</html:form>
	</div>
</body>
</html>