<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<html> 
<head>
<title><bean:message key="label.common.recent" /></title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script src="js/delete_news.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	function handler() {
		var localizedMessages = {
			CONFIRM_DELETE: "<bean:message key="confirm.delete" />",
			NOTHING_CHECK: "<bean:message key="error.message.nothing.checked" />"
		};
		return deleteFromListSome(localizedMessages);
	}
</script>
</head>
<body>
	<html:link action="/newsAction.do?method=news">
		<bean:message key="label.common.news" />
	</html:link>
	<bean:message key="label.common.section.list" />
	<div id="news-list">
		<html:form action="/newsAction.do?method=delete&list=true" method="post"
				onsubmit="return handler();">
			<logic:iterate id="element" name="newsForm" property="newsList">
				<br>
				<div id="news-list-title">
					<bean:write name="element" property="title" />
				</div>
				<div id="news-list-date">
					<bean:write name="element" property="date" formatKey="format.date" />
				</div>
				<br>
				<div id="news-list-brief">
					<bean:write name="element" property="brief" />
				</div>

				<div id="control">
					<html:link action="/newsAction.do?method=view" paramId="id"
						paramName="element" paramProperty="id">
						<bean:message key="label.common.viewtitle" />
					</html:link>

					<html:link action="/newsAction.do?method=edit&edit=true"
						paramId="id" paramName="element" paramProperty="id">
						<bean:message key="label.common.edit" />
					</html:link>

					<html:multibox property="selected">
						<bean:write name="element" property="id" format="0" />
					</html:multibox>
				</div>
				<br>
			</logic:iterate>
			<logic:notEmpty name="newsForm" property="newsList">
				<div id="delete">
					<html:submit styleId="button-delete">
						<bean:message key="label.common.button.delete" />
					</html:submit>
				</div>
			</logic:notEmpty>
		</html:form>
	</div>
</body>
</html>