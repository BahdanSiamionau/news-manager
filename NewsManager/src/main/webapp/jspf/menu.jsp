<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html>
<head></head>
<body>
	<h2>
		<bean:message key="label.common.news" />
	</h2>
	<ul>
		<li><html:link action="/newsAction.do?method=news">
				<bean:message key="label.common.list" />
			</html:link></li>
		<li><html:link action="/newsAction.do?method=edit&edit=false">
				<bean:message key="label.common.addnews" />
			</html:link></li>
	</ul>
</body>
</html>