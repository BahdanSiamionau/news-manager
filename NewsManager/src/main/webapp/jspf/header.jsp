<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head></head>
<body>
	<h1>
		<bean:message key="label.common.header" />
	</h1>

	<html:link page="/newsAction.do?method=language&locale=EN">
		<bean:message key="label.common.english" />
	</html:link>
	<html:link page="/newsAction.do?method=language&locale=RU">
		<bean:message key="label.common.russian" />
	</html:link>

</body>
</html>